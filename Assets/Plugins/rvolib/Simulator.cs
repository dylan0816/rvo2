﻿/*
 * RVO Library / RVO2 Library
 * 
 * Copyright ?2008-10 University of North Carolina at Chapel Hill. All rights 
 * reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its 
 * documentation for educational, research, and non-profit purposes, without 
 * fee, and without a written agreement is hereby granted, provided that the 
 * above copyright notice, this paragraph, and the following four paragraphs 
 * appear in all copies.
 * 
 * Permission to incorporate this software into commercial products may be 
 * obtained by contacting the University of North Carolina at Chapel Hill.
 * 
 * This software program and documentation are copyrighted by the University of 
 * North Carolina at Chapel Hill. The software program and documentation are 
 * supplied "as is", without any accompanying services from the University of 
 * North Carolina at Chapel Hill or the authors. The University of North 
 * Carolina at Chapel Hill and the authors do not warrant that the operation of 
 * the program will be uninterrupted or error-free. The end-user understands 
 * that the program was developed for research purposes and is advised not to 
 * rely exclusively on the program for any reason.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF NORTH CAROLINA AT CHAPEL HILL OR ITS 
 * EMPLOYEES OR THE AUTHORS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, 
 * SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE 
 * UNIVERSITY OF NORTH CAROLINA AT CHAPEL HILL OR THE AUTHORS HAVE BEEN ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF NORTH CAROLINA AT CHAPEL HILL AND THE AUTHORS SPECIFICALLY 
 * DISCLAIM ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND ANY 
 * STATUTORY WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE PROVIDED HEREUNDER IS ON 
 * AN "AS IS" BASIS, AND THE UNIVERSITY OF NORTH CAROLINA AT CHAPEL HILL AND THE 
 * AUTHORS HAVE NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, 
 * ENHANCEMENTS, OR MODIFICATIONS.
 */

using System.Collections.Generic;
using System.Threading;
using System;

namespace RVO
{
    public class Simulator
    {
        private static Simulator instance_ = new Simulator();

        private Agent defaultAgent_;
        private float time_;
        internal IList<Agent> agents_;
        internal IList<Obstacle> obstacles_;
        internal KdTree kdTree_;
        internal float timeStep_;

        private int _numWorkers;
		private Worker[] _workers;
        private ManualResetEvent[] _doneEvents;

        public static Simulator Instance { get { return instance_; } }
        private Simulator() { Clear(); }

        public void Clear()
        {
            agents_ = new List<Agent>();
            obstacles_ = new List<Obstacle>();
            time_ = 0;
            defaultAgent_ = null;
            kdTree_ = new KdTree();
            timeStep_ = .1f;

            SetNumWorkers(0);
        }

        public int GetNumWorkers()
        {
            return _numWorkers;
        }

        public void SetNumWorkers(int numWorkers)
        {
            _numWorkers = numWorkers;
            if (_numWorkers <= 0)
            {
                int completionPorts;
                ThreadPool.GetMinThreads(out _numWorkers, out completionPorts);
            }
            _workers = null;
        }

		/// <summary>
		/// Returns the global time of the simulation. 返回全局时间偏移//
		/// </summary>
		/// <returns>The global time.</returns>
        public float getGlobalTime() { return time_; }

		/// <summary>
		/// Returns the count of agents in the simulation. 返回 模拟器 下挂的 代理 数量//
		/// </summary>
		/// <returns>The number agents.</returns>
        public int getNumAgents() { return agents_.Count; }

		/// <summary>
		/// Returns the time step of the simulation. 返回模拟器的时间步长 (每步时间缩放,用于控制数度)//
		/// </summary>
		/// <returns>The time step.</returns>
        public float getTimeStep() { return timeStep_; }

		/// <summary>
		/// Sets the two-dimensional preferred velocity of a specified agent. 设置制定代理的标准2维速度 (normalize)//
		/// </summary>
		/// <param name="i">The index.</param>
		/// <param name="velocity">Velocity.</param>
        public void setAgentPrefVelocity(int i, Vector2 velocity)
        {
            agents_[i].prefVelocity_ = velocity;
        }

		/// <summary>
		/// Sets the time step of the simulation. 设置模拟器的时间步长 (每步时间缩放,用于控制数度)//
		/// </summary>
		/// <param name="timeStep">Time step.</param>
        public void setTimeStep(float timeStep)
        {
            timeStep_ = timeStep;
        }

		/// <summary>
		/// Returns the two-dimensional position of a specified agent. 
		/// 返回制定 代理 的位置 //
		/// </summary>
		/// <returns>The agent position.</returns>
		/// <param name="i">The index.</param>
        public Vector2 getAgentPosition(int i)
        {
            return agents_[i].position_;
        }

		/// <summary>
		/// Returns the two-dimensional preferred velocity of a specified agent.
		/// 返回制定 代理 的2维 的规范化速度(normalize)//
		/// </summary>
		/// <returns>The agent preference velocity.</returns>
		/// <param name="i">The index.</param>
        public Vector2 getAgentPrefVelocity(int i)
        {
            return agents_[i].prefVelocity_;
        }

		/// <summary>
		/// Returns the two-dimensional linear velocity of a specified agent. 
		/// 返回制定 代理 的 当前速度//
		/// </summary>
		/// <returns>The agent velocity.</returns>
		/// <param name="i">The index.</param>
        public Vector2 getAgentVelocity(int i)
        {
            return agents_[i].velocity_;
        }

		/// <summary>
		/// Returns the radius of a specified agent. 
		/// 返回制定代理的半径//
		/// </summary>
		/// <returns>The agent radius.</returns>
		/// <param name="i">The index.</param>
        public float getAgentRadius(int i)
        {
            return agents_[i].radius_;
        }

		/// <summary>
		/// Gets the agent orca lines.
		/// </summary>
		/// <returns>The agent orca lines.</returns>
		/// <param name="i">The index.</param>
        public IList<Line> getAgentOrcaLines(int i)
        {
            return agents_[i].orcaLines_;
        }

		/// <summary>
		/// Adds a new agent with default properties to the simulation. 
		/// 添加一个新的代理 至 模拟器 中//
		/// </summary>
		/// <returns>The agent.</returns>
		/// <param name="position">Position. 初始位置//</param>
        public int addAgent(Vector2 position)
        {
            if (defaultAgent_ == null)
            {
                return -1;
            }

            Agent agent = new Agent();

            agent.position_ = position;
            agent.maxNeighbors_ = defaultAgent_.maxNeighbors_;
            agent.maxSpeed_ = defaultAgent_.maxSpeed_;
            agent.neighborDist_ = defaultAgent_.neighborDist_;
            agent.radius_ = defaultAgent_.radius_;
            agent.timeHorizon_ = defaultAgent_.timeHorizon_;
            agent.timeHorizonObst_ = defaultAgent_.timeHorizonObst_;
            agent.velocity_ = defaultAgent_.velocity_;

            agent.id_ = agents_.Count;

            agents_.Add(agent);

            return agents_.Count - 1;

        }

		/// <summary>
		/// Sets the default properties for any new agent that is added. 
		/// 设置一个代理的默认属性，翻遍之后的默认添加 //
		/// </summary>
		/// <param name="neighborDist">Neighbor dist.</param>
		/// <param name="maxNeighbors">Max neighbors.</param>
		/// <param name="timeHorizon">Time horizon.</param>
		/// <param name="timeHorizonObst">Time horizon obst.</param>
		/// <param name="radius">Radius.</param>
		/// <param name="maxSpeed">Max speed.</param>
		/// <param name="velocity">Velocity.</param>
        public void setAgentDefaults(float neighborDist, int maxNeighbors, float timeHorizon, float timeHorizonObst, float radius, float maxSpeed, Vector2 velocity)
        {
            if (defaultAgent_ == null)
            {
                defaultAgent_ = new Agent();
            }

            defaultAgent_.maxNeighbors_ = maxNeighbors;
            defaultAgent_.maxSpeed_ = maxSpeed;
            defaultAgent_.neighborDist_ = neighborDist;
            defaultAgent_.radius_ = radius;
            defaultAgent_.timeHorizon_ = timeHorizon;
            defaultAgent_.timeHorizonObst_ = timeHorizonObst;
            defaultAgent_.velocity_ = velocity;
        }

        private class Worker
        {
            int _start;
            int _end;
            ManualResetEvent _doneEvent;

            internal Worker(int start, int end, ManualResetEvent doneEvent)
            {
                _start = start;
                _end = end;
                _doneEvent = doneEvent;
            }
            internal void step(object o)
            {
                for (int i = _start; i < _end; ++i)
                {
                    Simulator.Instance.agents_[i].computeNeighbors();
                    Simulator.Instance.agents_[i].computeNewVelocity();
                }
                _doneEvent.Set();
            }
            internal void update(object o)
            {
                for (int i = _start; i < _end; ++i)
                {
                    Simulator.Instance.agents_[i].update();
                }
                _doneEvent.Set();
            }
        }

		/// <summary>
		/// Lets the simulator perform a simulation step and updates 
		/// the two-dimensional position and two-dimensional velocity of each agent.
		/// 让模拟器执行模拟步骤和更新每个代理的二维位置和二维速度。 //
		/// </summary>
		/// <returns>The step.</returns>
        public float doStep()
        {
            if(_workers == null) 
            {
                _workers = new Worker[_numWorkers];
                _doneEvents = new ManualResetEvent[_workers.Length];
                for (int block = 0; block < _workers.Length; ++block)
                {
                    _doneEvents[block] = new ManualResetEvent(false);
                    _workers[block] = new Worker(block * getNumAgents() / _workers.Length, (block + 1) * getNumAgents() / _workers.Length, _doneEvents[block]);
                }
            }

            kdTree_.buildAgentTree();

            for (int block = 0; block < _workers.Length; ++block)
            {
                _doneEvents[block].Reset();
                ThreadPool.QueueUserWorkItem(_workers[block].step);
            }
            WaitHandle.WaitAll(_doneEvents);

            for (int block = 0; block < _workers.Length; ++block)
            {
                _doneEvents[block].Reset();
                ThreadPool.QueueUserWorkItem(_workers[block].update);
            }
            WaitHandle.WaitAll(_doneEvents);

            time_ += timeStep_;
            return time_;
        }

		/// <summary>
		/// Adds a new obstacle to the simulation. 添加一个仿真障碍//
		/// //
		/// </summary>
		/// <returns>The obstacle.</returns>
		/// <param name="vertices">Vertices.</param>
        public int addObstacle(IList<Vector2> vertices)
        {
            if (vertices.Count < 2)
            {
                return -1;
            }

            int obstacleNo = obstacles_.Count;

            for (int i = 0; i < vertices.Count; ++i)
            {
                Obstacle obstacle = new Obstacle();
                obstacle.point_ = vertices[i];
                if (i != 0)
                {
                    obstacle.prevObstacle = obstacles_[obstacles_.Count - 1];
                    obstacle.prevObstacle.nextObstacle = obstacle;
                }
                if (i == vertices.Count - 1)
                {
                    obstacle.nextObstacle = obstacles_[obstacleNo];
                    obstacle.nextObstacle.prevObstacle = obstacle;
                }
                obstacle.unitDir_ = RVOMath.normalize(vertices[(i == vertices.Count - 1 ? 0 : i + 1)] - vertices[i]);

                if (vertices.Count == 2)
                {
                    obstacle.isConvex_ = true;
                }
                else
                {
                    obstacle.isConvex_ = (RVOMath.leftOf(vertices[(i == 0 ? vertices.Count - 1 : i - 1)], vertices[i], vertices[(i == vertices.Count - 1 ? 0 : i + 1)]) >= 0);
                }

                obstacle.id_ = obstacles_.Count;

                obstacles_.Add(obstacle);
            }

            return obstacleNo;
        }

		/// <summary>
		/// Processes the obstacles that have been added so that they are accounted for in the simulation. 
		/// 在模拟器的障碍物进程上创建障碍物//
		/// </summary>
        public void processObstacles()
        {
            kdTree_.buildObstacleTree();
        }

		/// <summary>
		/// Performs a visibility query between the two specified points with respect to the obstacles. 
		/// 执行一个查询两个指定点之间的可见性的障碍//
		/// </summary>
		/// <returns><c>true</c>, if visibility was queryed, <c>false</c> otherwise.</returns>
		/// <param name="point1">Point1.</param>
		/// <param name="point2">Point2.</param>
		/// <param name="radius">Radius.</param>
        public bool queryVisibility(Vector2 point1, Vector2 point2, float radius)
        {
            return kdTree_.queryVisibility(point1, point2, radius);
        }

		/// <summary>
		/// Returns the count of obstacle vertices in the simulation.
		/// 返回障碍总数量//
		/// </summary>
		/// <returns>The number obstacle vertices.</returns>
        public int getNumObstacleVertices()
        {
            return obstacles_.Count;
        }

		/// <summary>
		/// Returns the two-dimensional position of a specified obstacle vertex.
		/// 返回制定索引的障碍物的位置//
		/// </summary>
		/// <returns>The obstacle vertex.</returns>
		/// <param name="vertexNo">Vertex no.</param>
        public Vector2 getObstacleVertex(int vertexNo)
        {
            return obstacles_[vertexNo].point_;
        }

		/// <summary>
		/// Returns the number of the obstacle vertex succeeding the specified obstacle vertex in its polygon. 
		/// ???????????????????????//
		/// </summary>
		/// <returns>The next obstacle vertex no.</returns>
		/// <param name="vertexNo">Vertex no.</param>
        public int getNextObstacleVertexNo(int vertexNo)
        {
            return obstacles_[vertexNo].nextObstacle.id_;
        }

		/// <summary>
		/// Returns the number of the obstacle vertex preceding the specified obstacle vertex in its polygon.
		/// ???????????????????????
		/// </summary>
		/// <returns>The previous obstacle vertex no.</returns>
		/// <param name="vertexNo">Vertex no.</param>
        public int getPrevObstacleVertexNo(int vertexNo)
        {
            return obstacles_[vertexNo].prevObstacle.id_;
        }
    }
}
