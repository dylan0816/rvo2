﻿using UnityEngine;
using System.Collections.Generic;
using RVO;

public class RvoMovement : IRVOMent{

	public List<RVO.Vector2> goals = new List<RVO.Vector2>();


	Simulator mSimulator;
	Simulator simulator
	{
		get{ 
			if(mSimulator == null)
				mSimulator = Simulator.Instance;
			return mSimulator;
		}
	}


	public void setupScenario()
	{
		simulator.addAgent(new RVO.Vector2(0,0));
		goals.Add(new RVO.Vector2(0,0));
	}

	public void setPreferredVelocities()
	{
		for (int i = 0; i < simulator.getNumAgents(); ++i)
		{
			RVO.Vector2 goalVector = goals[i] - Simulator.Instance.getAgentPosition(i);

		}
	}

	public void UpdateTarget(UnityEngine.Vector3 vec)
	{

	}

	public bool reachedGoal()
	{
		return false;
	}
}
