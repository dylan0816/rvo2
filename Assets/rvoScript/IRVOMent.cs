﻿using UnityEngine;
using System.Collections;

public interface IRVOMent{
	void setupScenario();
	void setPreferredVelocities();
	bool reachedGoal();
}
