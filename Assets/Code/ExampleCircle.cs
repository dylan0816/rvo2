using System.Collections.Generic;
using RVO;
using System;

public class ExampleCircle : IExample
{
    IList<Vector2> goals = new List<Vector2>();

    public void setupScenario()
    {
		UnityEngine.Debug.Log("ExampleCircle -> setupScenario 2 "+ Simulator.Instance.getNumAgents());
        /* Specify the global time step of the simulation. */
        Simulator.Instance.setTimeStep(0.25f);

        /* Specify the default parameters for agents that are subsequently added. */
        Simulator.Instance.setAgentDefaults(15.0f, 10, 10.0f, 10.0f, 1.5f, 2.0f, new Vector2());

        /* 
         * Add agents, specifying their start position, and store their goals on the
         * opposite side of the environment.
         */
        int numAgents = 5000;
        for (int i = 0; i < numAgents; ++i)
        {
            Simulator.Instance.addAgent(200.0f *
              new Vector2((float)Math.Cos(i * 2.0f * Math.PI / numAgents+Math.PI/2),
              (float)Math.Sin(i * 2.0f * Math.PI / numAgents+Math.PI/2)));
            goals.Add(-Simulator.Instance.getAgentPosition(i));
        }
		UnityEngine.Debug.Log("ExampleCircle -> setupScenario 2 "+ Simulator.Instance.getNumAgents());
    }

    public void UpdateVisualization()
    {
        /* Output the current global time. */
        System.Console.Write(Simulator.Instance.getGlobalTime() + " ");

        /* Output the current position of all the agents. */
        for (int i = 0; i < Simulator.Instance.getNumAgents(); ++i)
        {
            System.Console.Write(Simulator.Instance.getAgentPosition(i));
        }

        System.Console.WriteLine();
    }

    public void setPreferredVelocities()
    {
		//UnityEngine.Debug.Log("ExampleCircle -> setPreferredVelocities 1 ");
        /* 
         * Set the preferred velocity to be a vector of unit magnitude (speed) in the
         * direction of the goal.
         */
        for (int i = 0; i < Simulator.Instance.getNumAgents(); ++i)
        {
            if (RVOMath.absSq(Simulator.Instance.getAgentPosition(i) - goals[i]) < Simulator.Instance.getAgentRadius(i) * Simulator.Instance.getAgentRadius(i))
            {
                // Uncomment to ping-pong
                //goals[i] = -goals[i];
            }
            Vector2 goalVector = goals[i] - Simulator.Instance.getAgentPosition(i);
            if (RVOMath.absSq(goalVector) > 1.0f)
            {
                goalVector = RVOMath.normalize(goalVector);
            }
            Simulator.Instance.setAgentPrefVelocity(i, goalVector);
        }
		//UnityEngine.Debug.Log("ExampleCircle -> setPreferredVelocities 2 ");
    }

    public bool reachedGoal()
    {
        /* Check if all agents have reached their goals. */
        for (int i = 0; i < Simulator.Instance.getNumAgents(); ++i)
        {
            if (RVOMath.absSq(Simulator.Instance.getAgentPosition(i) - goals[i]) > Simulator.Instance.getAgentRadius(i) * Simulator.Instance.getAgentRadius(i))
            {
                return false;
            }
        }

        return true;
    }


}
