Shader "rvo-unity/Arrow" {
	Properties {
		_MainTex ("Main texture", 2D) = "" {}
	}
	SubShader {
		Pass {
			ZTest Always
			Cull Off
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			Fog { Mode Off }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD;
			};

			float4 _Color;
			sampler2D _MainTex;

			v2f vert (appdata v)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.color = v.color;
				o.uv = v.texcoord;
				return o;
			}

			half4 frag (v2f i) : COLOR
			{
				return i.color * tex2D(_MainTex, i.uv);
			}
			ENDCG

		}
	}
} 