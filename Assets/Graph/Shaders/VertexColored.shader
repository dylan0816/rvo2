Shader "rvo-unity/Line" {
	SubShader {
		Pass {
			ZTest Always
			Cull Off
			ZWrite Off
			Fog { Mode Off }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
			};

			float4 _Color;

			v2f vert (appdata v)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.color = v.color;
				return o;
			}

			half4 frag (v2f i) : COLOR
			{
				return i.color;
			}
			ENDCG

		}
	}
} 